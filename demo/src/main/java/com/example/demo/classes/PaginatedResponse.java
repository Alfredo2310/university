package com.example.demo.classes;

import java.util.List;

public class PaginatedResponse<T> {
	
	private List<T> items;
	private Integer page;
	private Long total;
	
	public PaginatedResponse(List<T> items, Integer page, Long total) {
		this.items = items;
		this.page = page;
		this.total = total;
	}
	
	public List<T> getItems() {
		return items;
	}
	public void setItems(List<T> items) {
		this.items = items;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	
	
}
