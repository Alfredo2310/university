package com.example.demo.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Segue{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "COURSE_ID")
	private long idCorso;
	
	@Column(name = "STUDENT_ID")
	private long idStudente;

	public Segue() {}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(long id_corso) {
		this.idCorso = id_corso;
	}

	public long getIdStudente() {
		return idStudente;
	}

	public void setIdStudente(long id_studente) {
		this.idStudente = id_studente;
	}
	
}
