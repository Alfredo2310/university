package com.example.demo.classes;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.example.demo.exception.BadRequestException;

@Entity
public class University {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String name;
	
	@Column 
	private String address;
	
	@Column
	private Long nStudents;
	
	@JoinColumn(name = "state_id")
	@ManyToOne(fetch = FetchType.EAGER)
	private State state;
	
	public University() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = name.trim();
		String app = name.substring(0,1).toUpperCase();
		name = name.substring(1,name.length()).toLowerCase(); 
		this.name = app + name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getnStudents() {
		return nStudents;
	}

	public void setnStudents(Long nStudents) {
		this.nStudents = nStudents;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void validateUni(long maxStud) throws BadRequestException {
		
		nStudents = Optional.ofNullable(nStudents).orElse(null);
		
		if(this.name == null) {
			throw new BadRequestException("Il nome dell'Università è obbligatorio");
		}else if(this.name.length() < 4 || this.name.length() > 20) {
			throw new BadRequestException("Nome Università non valido");
		}else if(nStudents > maxStud) {
			throw new BadRequestException(String.format("Il valore massimo per il numero degli iscritti è: %s", maxStud));
		}else if(this.state == null) {
			throw new BadRequestException("L'università deve appartenere ad uno stato");
		}else if(this.address != null) {
			if(this.address.length() < 6 || this.address.length() > 45) {
				throw new BadRequestException("L'indirizzo non è valido");
			}
		}
	}	
}
