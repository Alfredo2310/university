package com.example.demo.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.demo.exception.BadRequestException;
import com.example.demo.utils.PageUtils;

@Entity
@Table
public class State {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String code;
	
	@Column
	private String name;
	
	@Column
	private Long population;
	
	public State(){}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code.toUpperCase();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = PageUtils.format(name);
	}
	
	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}
	
	public void validateState() throws BadRequestException {
		
		if(code == null) {
			throw new BadRequestException("Il codice dello stato è obbligatorio");
		}else if(code.length() != 2) {
			throw new BadRequestException("Il codice dello Stato deve essere di due lettere");
		}else if(name == null) {
			throw new BadRequestException("Il nome dello stato è obbligatorio");
		}else if(name.length() < 3 || name.length() > 30) {
			throw new BadRequestException("Nome Stato non valido");
		}else if(population <= 0) {
			throw new BadRequestException("La popolazione non può essere minore di 0");
		}
	}
}