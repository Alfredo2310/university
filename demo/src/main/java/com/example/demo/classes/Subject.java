package com.example.demo.classes;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.demo.exception.BadRequestException;
import com.example.demo.utils.PageUtils;

@Table
@Entity
public class Subject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String codiceMateria;
	
	@Column
	private String nomeMateria;
	
	@Column(name = "materia_propedeutica")
	private String materiaPropedeutica;
	
	@Column
	private int cfu;
	
	public Subject() {}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCodiceMateria() {
		return codiceMateria;
	}

	public void setCodiceMateria(String codiceMateria) {
		this.codiceMateria = codiceMateria.replace(" ", "").toUpperCase();
	}

	public String getNomeMateria() {
		return nomeMateria;
	}

	public void setNomeMateria(String nomeMateria) {
		this.nomeMateria = PageUtils.format(nomeMateria);
	}

	public String getMateriaPropedeutica() {
		return materiaPropedeutica;
	}

	public void setMateriaPropedeutica(String materiaPropedeutica) {
		this.materiaPropedeutica = PageUtils.format(materiaPropedeutica);
	}

	public int getCfu() {
		return cfu;
	}

	public void setCfu(int cfu) {
		this.cfu = cfu;
	}

	public void validateSubject() throws BadRequestException {
		this.cfu = Optional.ofNullable(cfu).orElseThrow(() -> new BadRequestException("Ogni materia deve avere un cfu ( 3 / 6 / 9 / 12)"));
		
		if(this.nomeMateria == null) {
			throw new BadRequestException("Il nome della materia è obbligatorio");
		}else if(this.nomeMateria.length() < 6 || this.nomeMateria.length() > 20) {
			throw new BadRequestException("Nome Materia non valido");
		}else if(this.codiceMateria == null) {
			throw new BadRequestException("Il codice della materia è obbligatorio");
		}else if(this.codiceMateria.length() != 5) {
			throw new BadRequestException("Codice materia non valido");
		}else if(cfu != 3 && cfu != 6 && cfu != 9 && cfu != 12) {
			throw new BadRequestException("I cfu possono essere 3 / 6 / 9 / 12");
		}else if(this.materiaPropedeutica != null) {
			if(this.materiaPropedeutica.length() < 6 || this.materiaPropedeutica.length() > 20) {
				throw new BadRequestException("Nome Materia Propedeutica non valido");
			}
		}
	}
	
}
