package com.example.demo.classes;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.springframework.format.annotation.DateTimeFormat;
import com.example.demo.exception.BadRequestException;
import com.example.demo.utils.PageUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String matricola;
	
	@Column
	private String name;
	
	@Column
	private String surname;
	
	@JsonDeserialize
	@JsonSerialize
	@JsonFormat(pattern = "dd/MM/yyyy") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dateRegistration;
	
	@JsonDeserialize
	@JsonSerialize
	@JsonFormat(pattern = "dd/MM/yyyy") 
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate birth;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)  //esegue la query per prendersi l'universita solo quando si accede all'attributo università
	//@ManyToOne(fetch = FetchType.EAGER)  //esegue subito la query per prendersi l'università
	@JoinColumn(name = "university_id")
	private University university;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
	        name = "Segue", 
	        joinColumns = { @JoinColumn(name = "student_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "course_id") }
	    )
	private List<Course> courses;
	
	public Student() {}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getMatricola() {
		return matricola;
	}
	
	public void setMatricola(String matricola) {
		this.matricola = matricola.replace(" ", "").toUpperCase();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = PageUtils.format(name);
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = PageUtils.format(surname);
	}
	
	public LocalDate getDateRegistration() {
		return dateRegistration;
	}

	public void setDateRegistration(LocalDate dateRegistration) {
		this.dateRegistration = dateRegistration;
	}

	public LocalDate getBirth() {
		return birth;
	}

	public void setBirth(LocalDate birth) {
		this.birth = birth;
	}
	
	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public void validateStudent() throws BadRequestException {
		
		if(this.matricola == null) {
			throw new BadRequestException("La Matricola è obbligatoria");
		}else if(this.matricola.length() != 10 ) {
			throw new BadRequestException("La matricola non è valida");
		}else if(this.name == null) {
			throw new BadRequestException("Il Nome è obbligatorio");
		}else if((this.name.length() < 2 || this.name.length() > 20) || PageUtils.containsNumber(this.name)) {
			throw new BadRequestException("Nome non valido");
		}else if(this.surname == null) {
			throw new BadRequestException("Il Cognome è obbligatorio");
		}else if(this.surname.length() < 2 || this.surname.length() > 30) {
			throw new BadRequestException("Cognome non valido"); 
		}else if(this.birth == null) {
			throw new BadRequestException("La data di nascita è obbligatoria");
		}else if(!calcolaEta()) {
			throw new BadRequestException("Lo studente deve essere maggiorenne");
		}else if(dateRegistration != null) {
			if(dateRegistration.isAfter(LocalDate.now())) {
				throw new BadRequestException("La data d'iscrizione non può essere maggiore della data odierna");
			}
		}
	}
	
	public boolean calcolaEta() {
		LocalDate now = LocalDate.now();
	    if(now.getYear() - birth.getYear() > 17) {
	    	return true;
	    }else if(now.getYear() - birth.getYear() < 17) { return false;
	    }else {
	    		if(now.getMonthValue() < birth.getMonthValue()) return false;
	    		else if(now.getMonthValue() > birth.getMonthValue()) return true;
	    		else {
	    			if(now.getDayOfMonth() < birth.getDayOfMonth()) return false;
	    			else return true;
	    		}
	    	}
		}
	
	public void addCourse(Course c) {
		if(!this.courses.contains(c)) this.courses.add(c);
	}
	
	public void removeCourse(Course c) {
		if(this.courses.contains(c)) this.courses.remove(c);
	}
}
