package com.example.demo.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.exception.BadRequestException;
import com.example.demo.utils.PageUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private String name;
	
	@Column
	private String ambito;
	
	@Column
	private int durata;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "contiene",
			joinColumns = {@JoinColumn (name = "course_id") } ,
			inverseJoinColumns = {@JoinColumn( name = "subject_id") } )
	private List<Subject> subjectsList;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "university_id")
	private University university;
	
	public Course() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = name.trim();
		String app = name.substring(0,1).toUpperCase();
		name = name.substring(1,name.length()).toLowerCase();
		this.name = app + name;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = PageUtils.format(ambito);
	}

	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}

	public List<Subject> getSubjectsList() {
		return subjectsList;
	}

	public void setSubjectsList(List<Subject> subjectsList) {
		this.subjectsList = subjectsList;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}
	
	public void validateCourse(String ambito1, String ambito2, String ambito3) throws BadRequestException {
		this.durata = Optional.ofNullable(this.durata).orElseThrow(() -> new BadRequestException("Il corso deve avere una durata"));
		subjectsList = Optional.ofNullable(subjectsList).orElse(new ArrayList<>());
		
		if(this.name == null) {
			throw new BadRequestException("Il corso deve avere un nome");
		}else if(this.name.length() < 6 || this.name.length() > 45) {
			throw new BadRequestException("Nome corso non valido");
		}else if(this.ambito == null) {
			throw new BadRequestException("Il corso deve avere un ambito");
		}else if(!this.ambito.equalsIgnoreCase(ambito1) && !this.ambito.equalsIgnoreCase(ambito2) && !this.ambito.equalsIgnoreCase(ambito3)){
			throw new BadRequestException("Gli ambiti possibili sono: Scientifico, Umanitario e Professionale");
		}else if(this.university == null) {
			throw new BadRequestException("Il corso deve avere un università");
		}else if(this.durata != 3 && this.durata != 5 && this.durata != 2 && this.durata != 7 && this.durata != 10) {
			throw new BadRequestException("Durata corso non valida");
		}
	}
}
