package com.example.demo.controller;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.classes.LibUni;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.LibUniService;

@RestController
@RequestMapping("/api/libs")
public class LibUniController {
	
	private final LibUniService libUniService;
	
	@Autowired
	public LibUniController(LibUniService libUniService) {
		this.libUniService = libUniService;
	}


	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<LibUni> findList(@RequestParam(required = false) String subject,
											  @RequestParam(required = false) String course,
											  @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate date,
											  @RequestParam(required = false) String matricola,
											  @RequestParam(required = false) Integer page,
											  @RequestParam(required = false) Integer limit,
											  @RequestParam(required = false) String sort) throws NotFoundException, NoContentException{
		return libUniService.findList(subject, course, date, matricola, page, limit, sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public LibUni findById(@PathVariable("id") Long id) throws NotFoundException {
		return libUniService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public LibUni save(@RequestBody LibUni l) throws BadRequestException, NotFoundException, ConflictException {
		return libUniService.save(l);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public LibUni update(@PathVariable("id") Long id, @RequestBody LibUni l) throws Exception {
		return libUniService.update(id, l);
	}
	
	@DeleteMapping("student/{idS}/course/{idC}/subject/{idS}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("idS") Long idS, @PathVariable("idC") Long idC, @PathVariable("idM") Long idM) throws NotFoundException {
		libUniService.delete(idS, idC, idM);
	}
	
}
