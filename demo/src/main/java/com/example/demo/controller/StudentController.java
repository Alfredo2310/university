package com.example.demo.controller;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Student;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.StudentService;

@RestController
@RequestMapping("/api/students")
public class StudentController {
	
	private final StudentService studentService;

	@Autowired
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<Student> findList(@RequestParam(required = false) String matricola,
											   @RequestParam(required = false) String name,
											   @RequestParam(required = false) String surname,
											   @RequestParam(required = false) String nameUni,
											   @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate birth,
											   @RequestParam(required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate dateRegistration,
											   @RequestParam (required = false) Integer page, 
											   @RequestParam(required = false) Integer limit, 
											   @RequestParam(required = false) String sort) throws BadRequestException, NotFoundException, NoContentException{
		return studentService.findList(matricola, name, surname, nameUni, birth, dateRegistration, page, limit, sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Student findById(@PathVariable("id") long id) throws Exception {
		return studentService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Student save(@RequestBody Student s) throws BadRequestException, ConflictException, NotFoundException {
		return studentService.save(s);
	}
	
	@PostMapping("/bulk-insert")
	@ResponseStatus(code = HttpStatus.CREATED)
	public List<Student> saveMorePersons(@RequestBody Student... s) throws BadRequestException, ConflictException {
		List<Student> lists = new ArrayList<>();
		for(Student st : s) {
			lists.add(st);
		}
		return studentService.saveMoreStudent(lists);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Student update(@RequestBody Student s, @PathVariable("id") long id) throws Exception {
		return studentService.update(s, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") long id) throws NotFoundException {
		studentService.delete(id);
	}	
}
