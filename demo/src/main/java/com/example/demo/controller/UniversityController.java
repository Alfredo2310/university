package com.example.demo.controller;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.UniversityService;

@RestController
@RequestMapping("/api/universities")
public class UniversityController {

	private final UniversityService uniService;

	public UniversityController(UniversityService uniService) {
		this.uniService = uniService;
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<University> findList(@RequestParam(required = false) String address,
												  @RequestParam(required = false) String name,
												  @RequestParam(required = false) String codeS,
												  @RequestParam(required = false) Integer page, 
												  @RequestParam(required = false) Integer limit, 
												  @RequestParam(required = false) String sort) throws BadRequestException, NotFoundException, NoContentException{
		return uniService.findList(address, name, codeS, page, limit, sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public University findById(@PathVariable("id") long id) throws NotFoundException {
		return uniService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public University save(@RequestBody University u) throws BadRequestException, ConflictException, NotFoundException {
		return uniService.save(u);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public University update(@RequestBody University u, @PathVariable("id") int id) throws Exception {
		return uniService.update(u, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") int id) throws NotFoundException {
		uniService.delete(id);
	}
}
