package com.example.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Subject;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.SubjectService;

@RestController
@RequestMapping("/api/subjects")
public class SubjectController {
	
	private final SubjectService subjectService;

	@Autowired
	public SubjectController(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<Subject> findList(@RequestParam(required = false) String codiceMateria,
											   @RequestParam(required = false) String nomeMateria,
											   @RequestParam(required = false) Integer cfu,
											   @RequestParam(required = false) Integer page,
											   @RequestParam(required = false) Integer limit,
											   @RequestParam(required = false) String sort) throws NotFoundException, NoContentException{
		return subjectService.findList(codiceMateria, nomeMateria, cfu, page, limit, sort);
		
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Subject findById(@PathVariable("id") Long id) throws NotFoundException {
		return subjectService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Subject save(@RequestBody Subject s) throws BadRequestException, ConflictException {
		return subjectService.save(s);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Subject update(@PathVariable("id")Long id, @RequestBody Subject s) throws Exception {
		return subjectService.update(id, s);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) throws NotFoundException{
		subjectService.delete(id);
	}

}
