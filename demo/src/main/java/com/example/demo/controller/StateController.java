package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.State;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.StateService;

@RestController
@RequestMapping("/api/states")
public class StateController {
	
	private final StateService stateService;

	@Autowired
	public StateController(StateService stateService) {
		this.stateService = stateService;
	}

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<State> findList(@RequestParam(required = false) String code,
											 @RequestParam(required = false) String name,
											 @RequestParam(required = false) Long population,
											 @RequestParam(required = false) Integer page, 
											 @RequestParam(required = false) Integer limit, 
											 @RequestParam(required = false) String sort) throws BadRequestException, NotFoundException, NoContentException{
		return stateService.findList(code, name, population, page, limit, sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public State findById(@PathVariable("id") int id) throws NotFoundException {
		return stateService.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public State save(@RequestBody State s) throws BadRequestException, ConflictException {
		return stateService.save(s);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public State update(@RequestBody State s, @PathVariable("id") int id) throws Exception {
		return stateService.update(s, id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") int id) throws NotFoundException {
		stateService.delete(id);
	}

}
