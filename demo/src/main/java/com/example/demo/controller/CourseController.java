package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.classes.Contiene;
import com.example.demo.classes.Course;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Segue;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.service.ContieneService;
import com.example.demo.service.CourseService;
import com.example.demo.service.SegueService;

@RestController
@RequestMapping("/api/courses")
public class CourseController {
	
	private final CourseService courseService;
	private final SegueService segueService;
	private final ContieneService contieneService;

	@Autowired
	public CourseController(CourseService courseService, SegueService segueService, ContieneService contieneService) {
		this.courseService = courseService;
		this.segueService = segueService;
		this.contieneService = contieneService;
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<Course> findList(@RequestParam(required = false) String nomeCorso, 
											  @RequestParam(required = false) String ambito, 
											  @RequestParam(required = false) Integer durata, 
											  @RequestParam(required = false) String uniName,
											  @RequestParam(required = false) Integer page, 
											  @RequestParam(required = false) Integer limit, 
											  @RequestParam(required = false) String sort) throws BadRequestException, NotFoundException, NoContentException{
		return courseService.findList(nomeCorso, ambito, durata, uniName, page, limit, sort);
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Course findById(@PathVariable("id") Long id) throws NotFoundException {
		return courseService.findById(id);
	}
	
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Course save(@RequestBody Course c) throws BadRequestException, NotFoundException, ConflictException {
		return courseService.save(c);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public Course update(@PathVariable("id") Long id, @RequestBody Course c) throws Exception {
		return courseService.update(id, c);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") Long id) throws NotFoundException {
		courseService.delete(id);
	}
	
	@GetMapping("/segue")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<Segue> findAllCourseByStudent(@RequestParam(required = false) String matricola, 
														   @RequestParam(required = false) String nomeCorso,
														   @RequestParam(required = false) String sort,  
														   @RequestParam(required = false) Integer page,  
														   @RequestParam(required = false) Integer limit) throws Exception{
		return segueService.findAllCourseByStudent(matricola, nomeCorso, sort, page, limit);
	}
	
	@GetMapping("/{idC}/student/{idS}")
	@ResponseStatus(code = HttpStatus.OK)
	public Segue findByIdSegue(@PathVariable("idC") long idC, @PathVariable("idS") long idS) throws NotFoundException {
		return segueService.findById(idC, idS);
	}
	
	@PostMapping("/follow/student")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Segue addStud(@RequestBody Segue s) throws NotFoundException, BadRequestException, ConflictException {
		return segueService.save(s);
	}
	
	@DeleteMapping("/{idC}/student/{idS}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void removeStud(@PathVariable("idC") long idC, @PathVariable("idS") long idS) throws NotFoundException {
		segueService.remove(idC, idS);
	}
	
	@GetMapping("/contains/subject")
	@ResponseStatus(code = HttpStatus.OK)
	public PaginatedResponse<Contiene> findList(@RequestParam(required = false) String nameCorso, 
												@RequestParam(required = false) String codeSub, 
												@RequestParam(required = false) String nameUni, 
												@RequestParam(required = false) Integer page, 
												@RequestParam(required = false) Integer limit, 
												@RequestParam(required = false) String sort) throws BadRequestException, NotFoundException, NoContentException{
		return contieneService.findList(nameCorso, codeSub, nameUni, page, limit, sort);
	}
	
	@GetMapping("/{idC}/contains/{idM}")
	@ResponseStatus(code = HttpStatus.OK)
	public Contiene findByIdContains(@PathVariable("idC") long idC, @PathVariable("idM") long idM) throws NotFoundException {
		return contieneService.findById(idC, idM);
	}
	
	@PostMapping("/contains")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Contiene addSubject(@RequestBody Contiene c) throws NotFoundException, ConflictException, BadRequestException {
		return contieneService.add(c);
	}
	
	@DeleteMapping("/{idC}/contains/{idM}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void removeSub(@PathVariable("idC") Long idC, @PathVariable("idM") Long idM) throws NotFoundException {
		contieneService.remove(idC, idM);
	}
	
}
