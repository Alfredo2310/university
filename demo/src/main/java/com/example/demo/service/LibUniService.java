package com.example.demo.service;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.example.demo.classes.LibUni;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface LibUniService {
	
	PaginatedResponse<LibUni> findList(String sub, String course, LocalDate date, String matricola, Integer page, Integer limit, String sort) throws NotFoundException, NoContentException;
	LibUni findById(Long id) throws NotFoundException;
	LibUni save(LibUni l) throws BadRequestException, NotFoundException, ConflictException;
	LibUni update(Long id, LibUni l) throws Exception;
	void delete(Long idS, Long idC, Long idM) throws NotFoundException;

}
