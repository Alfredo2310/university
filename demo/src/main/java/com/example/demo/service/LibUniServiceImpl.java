package com.example.demo.service;

import java.time.LocalDate;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.classes.Course;
import com.example.demo.classes.LibUni;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Student;
import com.example.demo.classes.Subject;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.LibUniRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.repository.UniversityRepository;
import com.example.demo.utils.PageUtils;

@Service
public class LibUniServiceImpl implements LibUniService{
	
	private final LibUniRepository libUniRepository;
	private final CourseRepository courseRepository;
	private final SubjectRepository subjectRepository;
	private final StudentRepository studentReposity;
	private final UniversityRepository uniRepository;
	
	@Autowired
	public LibUniServiceImpl(UniversityRepository uniRepository, StudentRepository studentReposity, LibUniRepository libUniRepository,  CourseRepository courseRepository, SubjectRepository subjectRepository) {
		this.courseRepository =  courseRepository;
		this.libUniRepository = libUniRepository;
		this.subjectRepository = subjectRepository;
		this.studentReposity = studentReposity;
		this.uniRepository = uniRepository;
	}

	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;

	@Override
	public PaginatedResponse<LibUni> findList(String subject, String course, LocalDate date, String matricola, Integer page, Integer limit, String sort) throws NotFoundException, NoContentException {
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else { 
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(matricola != null) {
			Student stud = studentReposity.findFirstByMatricola(matricola);
			if(stud == null) throw new NotFoundException("Lo studente non esiste");
		}
		
		if(course != null) {
			Course cor = courseRepository.findFirstByName(course);
			if(cor == null) throw new NotFoundException("Il corso non esiste");
		}
		
		if(subject != null) {
			Subject materia = subjectRepository.findByCodiceMateria(subject);
			if(materia == null) throw new NotFoundException("La materia non esiste");
		}
		
		Page<LibUni> result = libUniRepository.findQuery(subject, course, date, matricola, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono ancora presenti libretti");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
		
	}

	@Override
	public LibUni findById(Long id) throws NotFoundException {
		return Optional.ofNullable(libUniRepository.findFirstById(id))
					   .orElseThrow(() -> new NotFoundException(String.format("Il libretto con id %s è inesistente", id)));
	}

	@Override
	public LibUni save(LibUni l) throws BadRequestException, NotFoundException, ConflictException {
		//Cerco lo studente
		Student s = studentReposity.findFirstByMatricola(l.getStudent().getMatricola());
		if(s == null) throw new NotFoundException("Lo studente non esiste");
		l.setStudent(s);
		
		//Cerco l'università per andare a trovare il corso specifico (dato che il nome corso non è univoco)
		University u = uniRepository.findFirstByName(l.getCourse().getUniversity().getName());
		if(u == null) throw new BadRequestException(String.format("'L'università %s non esiste", l.getCourse().getUniversity().getName()));
		//Cerco il corso
		Course c = courseRepository.findFirstByNameAndUniversityName(l.getCourse().getName(), u.getName());
		if(c == null) throw new NotFoundException("Il corso non esiste");
		c.setUniversity(u);
		l.setCourse(c);
		
		//lo Studente non è iscritto alla stessa università del corso
		if(!l.getStudent().getUniversity().getName().equalsIgnoreCase(l.getCourse().getUniversity().getName())) throw new BadRequestException("Lo studente è iscritto ad un altra Università");
		
		//Cerco la materia
		Subject sub =	subjectRepository.findByCodiceMateria(l.getSubject().getCodiceMateria());	
		if(sub == null) throw new NotFoundException("La materia non esiste");
		l.setSubject(sub);
		
		//Controllo campi esame
		l.validateLibUni();
		
		//Errore materia non fa parte del corso
		if(!c.getSubjectsList().contains(sub)) throw new BadRequestException(String.format("La materia %s non fa parte del corso %s", sub.getNomeMateria(), c.getName()));
		
		//Errore studente non frequenta il corso
		if(!s.getCourses().contains(c)) throw new BadRequestException(String.format("Lo studente %s non è iscritto al corso %s", s.getMatricola(), c.getName()));
		
		//controllo esame
		LibUni prev = libUniRepository.findExam(l.getStudent().getId(), l.getCourse().getId(), l.getSubject().getId());
		//esame non dato
		if(prev == null) {
			//controllo materia propedeutica
			if(l.getSubject().getMateriaPropedeutica() != null) {
				String prop = l.getSubject().getMateriaPropedeutica();
				prev = libUniRepository.findExamPassed(prop, l.getStudent().getId(), l.getCourse().getId());
				if(prev == null) throw new BadRequestException("Non puoi dare questo esame perchè non hai la propedeuticità della materia");
			}
			//esame senza materia propedeutica
		}else {
			//esame dato verifichiamo il voto
			if(prev.getVoto() > 17) {
				throw new ConflictException(String.format("Lo studente %s ha già dato la materia %s con voto > 17", prev.getStudent().getMatricola(), prev.getSubject().getNomeMateria()));
			}
		}
		
		return libUniRepository.save(l);
	}

	@Override
	public LibUni update(Long id, LibUni l) throws Exception {
		//controllo valori esame
		l.validateLibUni();
		
		//cerco se esiste
		LibUni prev = findById(id);
		//if(prev.getVoto() != l.getVoto()) throw new BadRequestException("Non puoi modificare il voto di un esame");
		
		//Cerco lo studente
		Student student = studentReposity.findFirstByMatricola(l.getStudent().getMatricola());
		if(student == null) throw new NotFoundException(String.format("Lo studente %s non esiste", l.getStudent().getMatricola()));
		//if(!prev.getStudent().getMatricola().equalsIgnoreCase(l.getStudent().getMatricola())) throw new ForbiddenException("Non sei autorizzato ad aggiornare altri studenti");
		l.setStudent(student);
		
		//Cerco il corso
		Course cours =  courseRepository.findFirstByNameAndUniversityName(l.getCourse().getName(), l.getCourse().getUniversity().getName());
		if(cours == null) throw new NotFoundException("Il corso non esiste");
		l.setCourse(cours);
		
		//Controllo se lo studente è iscritto al corso
		if(!l.getCourse().getUniversity().getName().equalsIgnoreCase(l.getStudent().getUniversity().getName())) throw new BadRequestException(String.format("Lo studente %s non è iscritto al corso %s", prev.getStudent().getMatricola(), l.getCourse().getName()));
		
		//Cerco la materia
		Subject s = subjectRepository.findByCodiceMateria(l.getSubject().getCodiceMateria());
		if(s == null) throw new NotFoundException(String.format("La materia con codice %s non esiste", l.getSubject().getCodiceMateria()));
		l.setSubject(s);
		
		/*	Non controllo se la materia fa parte del corso perche possiamo aggiornare solo esami dati dallo studente 
			quindi a controllare se la materia non fa parte del corso ci pensa la save
			(se la materia non fa parte del corso lo studente non puo averla data)									*/
		
		//Cerco se lo studente ha già dato la materia
		prev = libUniRepository.findExam(l.getStudent().getId(), l.getCourse().getId(), l.getSubject().getId());
		if(prev == null) throw new ConflictException(String.format("Lo studente con matricola %s non ha mai dato la materia %s", l.getStudent().getMatricola(), l.getSubject().getNomeMateria())); 
		
		
		l.setId(id);
		return libUniRepository.save(l);
	}

	@Transactional
	@Override
	public void delete(Long idS, Long idC, Long idM) throws NotFoundException {
		findById(idS);
		
		libUniRepository.deleteByStudentIdAndCourseIdAndSubjectId(idS, idC, idM);
	}

	
}
