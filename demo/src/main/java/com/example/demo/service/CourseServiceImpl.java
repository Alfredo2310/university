package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.classes.Course;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.UniversityRepository;
import com.example.demo.utils.PageUtils;

@Service
public class CourseServiceImpl implements CourseService{
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	@Value("${demo.ambitoPro}")
	private String ambito1;
	
	@Value("${demo.ambitoScie}")
	private String ambito2;
	
	@Value("${demo.ambitoUma}")
	private String ambito3;
	
	private final CourseRepository courseRepository;
	private final UniversityRepository universityRepository;
	
	@Autowired
	public CourseServiceImpl(CourseRepository courseRepository, UniversityRepository universityRepository) {
		this.courseRepository = courseRepository;
		this.universityRepository = universityRepository;
	}

	@Override
	public PaginatedResponse<Course> findList(String nomeCorso, String ambito, Integer durata, 
			String uniName, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException {
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("Il valore massimo per limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(nomeCorso != null) {
			Course c = courseRepository.findFirstByName(nomeCorso);
			if(c == null) throw new NotFoundException(String.format("Il corso %s non esiste", nomeCorso));
			nomeCorso = "%" + nomeCorso + "%";
		}
		if(ambito != null) {
			Course c = courseRepository.findByAmbito(ambito);
			if(c == null) throw new NotFoundException(String.format("L'ambito %s non esiste", ambito));
		}
		if(durata != null) {
			Course c = courseRepository.findByDurata(durata);
			if(c == null) throw new NotFoundException(String.format("La durata %s non esiste", durata));
		}
		if(uniName != null) {
			University u = universityRepository.findFirstByName(uniName);
			if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", uniName));
		}
		
		Page<Course> result = courseRepository.findQuery(nomeCorso, ambito, durata, uniName, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono presenti ancora nessun corso");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}

	@Override
	public Course findById(Long id) throws NotFoundException {
		return Optional.ofNullable(courseRepository.findFirstById(id))
		.orElseThrow(() -> new NotFoundException(String.format("Il corso con id %s non esiste", id)));
		
	}

	@Override
	public Course save(Course c) throws BadRequestException, NotFoundException, ConflictException {
		c.validateCourse(ambito1, ambito2, ambito3);
		
		University u = universityRepository.findFirstByName(c.getUniversity().getName());
		if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", c.getUniversity().getName()));
		c.setUniversity(u);
		
		Course prev = courseRepository.findFirstByNameAndUniversityNameAndIdIsNot(c.getName(), c.getUniversity().getName(), c.getId());
		PageUtils.isExisting(prev);
		
		return courseRepository.save(c);
	}

	@Override
	public Course update(Long id, Course c) throws Exception{
		Course prev = findById(id);
		University u = universityRepository.findFirstById(prev.getUniversity().getId());
		if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", c.getUniversity().getName()));
		c.setUniversity(u);
		
		if(!(prev.getName() + prev.getUniversity().getName()).equalsIgnoreCase((c.getName() + c.getUniversity().getName()))) {
			prev = courseRepository.findFirstByNameAndUniversityNameAndIdIsNot(c.getName(), c.getUniversity().getName(), c.getId());
			PageUtils.isExisting(prev);
		}
		
		c.setId(id);
		c = save(c);
		return c;
	}

	@Transactional
	@Override
	public void delete(Long id) throws NotFoundException {
		findById(id);
		
		courseRepository.deleteById(id);
		
	}

}
