package com.example.demo.service;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

public interface UniversityService {
	//query by method
	PaginatedResponse<University> findList(String address, String name, String codeS, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException;
	University findById(long id) throws NotFoundException;
	University save(University s) throws BadRequestException, ConflictException, NotFoundException;
	University update(University s, long id) throws Exception;
	void delete(long id) throws NotFoundException;
}
