package com.example.demo.service;


import java.time.LocalDate;
import java.util.List;
import org.springframework.stereotype.Service;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Student;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface StudentService {

	PaginatedResponse<Student> findList(String matricola, String name, String surname, String nameUni, LocalDate birth, LocalDate dateRegistration, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException;
	Student findById(long id) throws Exception;
	Student save(Student s) throws BadRequestException, ConflictException, NotFoundException;
	List<Student> saveMoreStudent(List<Student> s) throws BadRequestException, ConflictException;
	Student update(Student s, long id) throws Exception;
	void delete(long id) throws NotFoundException;
}
