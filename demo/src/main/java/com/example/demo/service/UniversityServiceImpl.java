package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.State;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.StateRepository;
import com.example.demo.repository.UniversityRepository;
import com.example.demo.utils.PageUtils;

@Service
public class UniversityServiceImpl implements UniversityService{
	
	private final UniversityRepository uniRepository;
	private final StateRepository stateRepository;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Value("${demo.max-stud}")
	private long maxStud;
	
	@Autowired
	public UniversityServiceImpl(UniversityRepository uniRepository, StateRepository stateRepository) {
		this.uniRepository = uniRepository;
		this.stateRepository = stateRepository;
	}

	@Override
	public PaginatedResponse<University> findList(String address, String name, 
												  String codeS, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException {
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("Il valore massimo pe il limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(name != null) {
			University u = uniRepository.findFirstByName(name);
			if(u == null) throw new NotFoundException(String.format("L'università con nome %s non esiste", name));
		}

		if(codeS != null) {
			State u = stateRepository.findFirstByCode(codeS);
			if(u == null) throw new NotFoundException(String.format("Lo stato con nome %s non esiste", codeS));
		}
		
		if(address != null) {
			University u = uniRepository.findFirstByAddress(address);
			if(u == null) throw new NotFoundException(String.format("L'università in  %s non esiste", address));
		}
		
		Page<University> result = uniRepository.findQuery(address, name, codeS, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono ancora presenti università");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}

	@Override
	public University findById(long id) throws NotFoundException {
		return  Optional.ofNullable(uniRepository.findFirstById(id))
						.orElseThrow(()-> new NotFoundException(String.format("L'id %s è inesistente", id)));
	}

	@Override
	public University save(University s) throws BadRequestException, ConflictException, NotFoundException {
		s.validateUni(maxStud);
		
		State st = stateRepository.findFirstByCode(s.getState().getCode());
		if(st == null) throw new NotFoundException(String.format("Lo stato %s è inesistente", s.getState().getCode()));
		s.setState(st);
		
		University u = uniRepository.findFirstByNameAndIdIsNot(s.getName(), Optional.ofNullable(s.getId()).orElse(0l));
		u = (University) PageUtils.isExisting(u);
		
		return uniRepository.save(s);
	}

	@Override
	public University update(University s, long id) throws NotFoundException, BadRequestException, ConflictException {
		University prev = findById(id);
		
		if(!prev.getName().equalsIgnoreCase(s.getName())) {
			prev = uniRepository.findFirstByNameAndIdIsNot(s.getName(), Optional.ofNullable(id).orElse(0l));
			if(prev != null) throw new ConflictException("L'università è già esistente");
		}
		s.setId(id);
		s = save(s);
		return s;
	}

	@Transactional
	@Override
	public void delete(long id) throws NotFoundException {
		findById(id);
		
		uniRepository.deleteById(id);
	}
}
