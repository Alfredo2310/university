package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.utils.*;
import com.example.demo.classes.Contiene;
import com.example.demo.classes.Course;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Subject;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.ContieneRepository;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.repository.UniversityRepository;

@Service
public class ContieneServiceImpl implements ContieneService{
	
	public final ContieneRepository contieneRepository;
	public final CourseRepository courseRepository;
	public final SubjectRepository subjectRepository;
	private final UniversityRepository uniRepository;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	@Autowired
	public ContieneServiceImpl(ContieneRepository contieneRepository, 
							   CourseRepository courseRepository, 
							   SubjectRepository subjectRepository,
							   UniversityRepository uniRepository) {
		this.subjectRepository = subjectRepository;
		this.courseRepository = courseRepository;
		this.contieneRepository = contieneRepository;
		this.uniRepository = uniRepository;
	}
	
	@Override
	public PaginatedResponse<Contiene> findList(String nameCorso, String codeSub, String nameUni, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException {
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("La dimensione massima per il limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s == null) {
			pageRequest = PageRequest.of(page, limit);
		}else {
			pageRequest = PageRequest.of(page, limit, s);
		}
		
		if(nameCorso != null) {
			Course c = courseRepository.findFirstByName(nameCorso);
			if(c == null) throw new NotFoundException(String.format("Il corso %s non esiste", nameCorso));
		}
		
		if(nameUni != null) {
			University u = uniRepository.findFirstByName(nameUni);
			if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", nameUni));
		}
		
		if(codeSub != null) {
			Subject sub = subjectRepository.findByCodiceMateria(codeSub);
			if(sub == null) throw new NotFoundException(String.format("La materia %s non esiste", codeSub));
		}
		
		Page<Contiene> result = contieneRepository.findQuery(nameCorso, codeSub, nameUni, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono presenti ancora nessun associazione tra corso e materia");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}

	@Override
	public Contiene findById(Long idC, Long idM) throws NotFoundException {
		return Optional.ofNullable(contieneRepository.findFirstByIdCorsoAndIdMateria(idC, idM))
					   .orElseThrow(() -> new NotFoundException(String
							   									.format("Non c'è nessuna associazione tra il corso con id %s e la materia con id %s", idC, idM)));
	}

	@Override
	public Contiene add(Contiene c) throws NotFoundException, ConflictException, BadRequestException {
		if(c.getIdCorso() == 0) throw new BadRequestException("Campo id corso non valorizzato");
		if(c.getIdMateria() == 0) throw new BadRequestException("Campo id materia non valorizzato");
		
		Course prev = courseRepository.findFirstById(c.getIdCorso());
		if(prev == null) throw new NotFoundException(String.format("Il corso con id %s non esiste", c.getIdCorso()));
		Subject prev2 = subjectRepository.findFirstById(c.getIdMateria());
		if(prev2 == null) throw new NotFoundException(String.format("La materia con id %s non esiste", c.getIdMateria()));
		
		Contiene app = contieneRepository.findFirstByIdCorsoAndIdMateria(c.getIdCorso(), c.getIdMateria());
		if(app != null) throw new ConflictException("E' già presente un associazione tra questo corso e questa materia");
		
		return contieneRepository.save(c);
	}

	@Transactional
	@Override
	public void remove(Long idC, Long idM) throws NotFoundException {
		Contiene c =findById(idC, idM);
		
		Subject s = subjectRepository.findFirstById(idM);
		Course cou = courseRepository.findFirstById(idC);
		
		if(s == null) throw new NotFoundException("La materia non esiste");
		if(cou == null) throw new NotFoundException("Il corso non esiste");
		
		//cou.removeSubject(s);
		
		contieneRepository.delete(c);
		
	}
}
