package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.classes.Course;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface CourseService {
	PaginatedResponse<Course> findList(String nomeCorso, String ambito, Integer durata, String uniName, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException;
	Course findById(Long id) throws NotFoundException;
	Course save(Course c) throws BadRequestException, NotFoundException, ConflictException;
	Course update(Long id, Course c) throws Exception;
	void delete(Long id) throws NotFoundException;
}
