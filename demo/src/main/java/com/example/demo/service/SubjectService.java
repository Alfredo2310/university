package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Subject;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface SubjectService {
	PaginatedResponse<Subject> findList(String codiceMateria, String nomeMateria, Integer cfu, Integer page, Integer limit, String sort) throws NotFoundException, NoContentException;
	Subject findById(long id) throws NotFoundException;
	Subject save(Subject s) throws BadRequestException, ConflictException;
	Subject update(long id, Subject s) throws Exception;
	void delete(long id) throws NotFoundException;
	
}
