package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.State;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.StateRepository;
import com.example.demo.utils.PageUtils;

@Service
public class StateServiceImpl implements StateService{
	
	private final StateRepository stateRepository;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Autowired
	public StateServiceImpl(StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

	@Override
	public PaginatedResponse<State> findList(String code, String name, Long population, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException {
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		population = Optional.ofNullable(population).orElse(0L);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("Il valore massimo per il limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(code != null) {
			State state = stateRepository.findFirstByCode(code);
			if(state == null) throw new NotFoundException(String.format("Lo stato coon codice %s non esiste", code));
		}
		
		if(name != null) {
			State state = stateRepository.findFirstByName(name);
			if(state == null) throw new NotFoundException(String.format("Lo stato coon nome %s non esiste", name));
		}
		
		Page<State> result = stateRepository.findQuery(code, name, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono ancora presenti stati");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
		
	}
	
	@Override
	public State findById(long id) throws NotFoundException  {
		return Optional.ofNullable(stateRepository.findFirstById(id)).orElseThrow(()-> new NotFoundException(String.format("Lo stato con id %s non è esistente", id)));
	}

	@Override
	public State save(State s) throws BadRequestException, ConflictException {
		s.validateState();
		State state = stateRepository.findFirstByCodeAndIdIsNot(s.getCode(), Optional.ofNullable(s.getId()).orElse(0l));
		PageUtils.isExisting(state);
		
		return stateRepository.save(s);
	}

	@Override
	public State update(State s, long id) throws NotFoundException, BadRequestException, ConflictException {
		State state = findById(id);
		if(state.getPopulation() > s.getPopulation()) throw new BadRequestException("La popolazione può essere solo incrementata");
		
		if(!state.getName().equalsIgnoreCase(s.getName())) {
			state = stateRepository.findFirstByCodeAndIdIsNot(s.getCode(), Optional.ofNullable(id).orElse(0l));
			if(state != null) throw new ConflictException("Lo Stato è già esistente");
		}
		
		s.setId(id);
		return save(s);
	}

	@Transactional
	@Override
	public void delete(long id) throws NotFoundException {
		findById(id);
		
		stateRepository.deleteById(id);
	}

}
