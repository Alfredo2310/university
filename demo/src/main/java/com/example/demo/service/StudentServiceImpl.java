package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Student;
import com.example.demo.classes.University;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.StudentRepository;
import com.example.demo.repository.UniversityRepository;
import com.example.demo.utils.PageUtils;

@Service
public class StudentServiceImpl implements StudentService{
	
	private final StudentRepository studentRepository;
	public final UniversityRepository universityRepository;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, UniversityRepository universityRepository) {
		this.studentRepository = studentRepository;
		this.universityRepository = universityRepository;
	}

	@Override
	public PaginatedResponse<Student> findList(String matricola, String name, String surname, String uniName, LocalDate birth, LocalDate dateRegistration, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException{	
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		matricola = Optional.ofNullable(matricola).map(m -> "%" + m + "%").orElse(null);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("Il valore massimo per limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(name != null) {
			Student stu = studentRepository.findFirstByName(name);
			if(stu == null) throw new NotFoundException(String.format("Lo studente con nome %s non esiste", name));
		}
		
		if(surname != null) {
			Student stu = studentRepository.findFirstBySurname(surname);
			if(stu == null) throw new NotFoundException(String.format("Lo studente con cognome %s non esiste", surname));
		}
		
		if(matricola != null) {
			Student stu = studentRepository.findFirstByMatricola(matricola);
			if(stu == null) throw new NotFoundException(String.format("Lo studente con matricola %s non esiste", matricola));
		}
		
		if(birth != null) {
			Student stu = studentRepository.findFirstByBirth(birth);
			if(stu == null) throw new NotFoundException(String.format("Lo studente con data nascita %s non esiste", birth));
		}
		
		if(dateRegistration != null) {
			Student stu = studentRepository.findFirstByBirth(dateRegistration);
			if(stu == null) throw new NotFoundException(String.format("Lo studente con data registrazione %s non esiste", dateRegistration));
		}
		
		Page<Student> result = studentRepository.findQuery(matricola, name, surname, uniName, birth, dateRegistration, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono ancora presenti studenti");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}

	@Override
	public Student findById(long id) throws NotFoundException {
		return Optional.ofNullable(studentRepository.findFirstById(id)).orElseThrow(()->new NotFoundException(String.format("Utente con id %s non trovato", id)));
	}

	@Override
	public Student save(Student s) throws BadRequestException, ConflictException, NotFoundException {
		s.validateStudent();
		Student s1 = studentRepository.findFirstByMatricola(s.getMatricola());
		
		if(s1 != null) {
			throw new BadRequestException("La matricola è già esistente");
		}
			
		University u = universityRepository.findFirstByName(s.getUniversity().getName());
		if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", s.getUniversity().getName()));
		s.setUniversity(u);
		
		return studentRepository.save(s);
	}

	@Override
	public Student update(Student s, long id) throws NotFoundException, BadRequestException, ConflictException {
		Student stud = findById(id);
		University u = universityRepository.findFirstById(stud.getUniversity().getId());
		
		if(!stud.getMatricola().equalsIgnoreCase(s.getMatricola())) {
			stud = studentRepository.findByMatricolaAndIdIsNot(s.getMatricola(), Optional.ofNullable(id).orElse(0l));
			if(stud != null) throw new ConflictException("Lo Studente è già esistente");
		}
		if(!u.getName().equalsIgnoreCase(s.getUniversity().getName())) {
			u = universityRepository.findFirstByNameAndIdIsNot(s.getUniversity().getName(), s.getUniversity().getId());
			if(u == null) throw new NotFoundException(String.format("L'università %s non esiste", s.getUniversity().getName()));
		}
		s.setUniversity(u);
		
		s.setId(id);
		return studentRepository.save(s);
	}

	@Transactional
	@Override
	public void delete(long id) throws NotFoundException {
		findById(id);
		
		studentRepository.deleteById(id);
	}
	
	@Transactional
	@Override
	public List<Student> saveMoreStudent(List<Student> s) throws BadRequestException, ConflictException {
		List<Student> studReject = new ArrayList<>();
		for(Student s1 : s) {
			try {
				studentRepository.save(s1);
			}catch (Exception e) {
				studReject.add(s1);
			}
		}
		
		return studReject;
	}

}
