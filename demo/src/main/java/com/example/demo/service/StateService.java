package com.example.demo.service;


import org.springframework.stereotype.Service;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.State;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface StateService {
	
	PaginatedResponse<State> findList(String code, String name, Long population, Integer page, Integer size, String sort) throws BadRequestException, NotFoundException, NoContentException;
	State findById(long id) throws NotFoundException;
	State save(State s) throws BadRequestException, ConflictException;
	State update(State s, long id) throws Exception;
	void delete(long id) throws NotFoundException;

}
