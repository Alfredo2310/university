package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.classes.Contiene;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@Service
public interface ContieneService {

	PaginatedResponse<Contiene> findList(String nameCorso, String codeSub, String nameUni, Integer page, Integer limit, String sort) throws BadRequestException, NotFoundException, NoContentException;
	Contiene findById(Long idC, Long idM) throws NotFoundException;
	Contiene add(Contiene c) throws NotFoundException, ConflictException, BadRequestException;
	void remove(Long idC, Long idM) throws NotFoundException;
	
}
