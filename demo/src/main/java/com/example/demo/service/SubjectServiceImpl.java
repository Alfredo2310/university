package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Subject;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.utils.PageUtils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SubjectServiceImpl implements SubjectService{
	
	private final SubjectRepository subjectRepository;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	@Value("${demo.max-limit}")
	private Integer maxLimit;

	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository) {
		this.subjectRepository = subjectRepository;
	}

	@Override
	public PaginatedResponse<Subject> findList(String codiceMateria, String nomeMateria, Integer cfu, Integer page,
			Integer limit, String sort) throws NotFoundException, NoContentException {
		
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		if(limit > maxLimit) limit = defaultLimit;
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(nomeMateria != null) {
			Subject sub = subjectRepository.findFirstBynomeMateria(nomeMateria);
			if(sub == null) throw new NotFoundException(String.format("La materia %s non esiste", nomeMateria));
		}
		if(codiceMateria != null) {
			Subject sub = subjectRepository.findByCodiceMateria(codiceMateria);
			if(sub == null) throw new NotFoundException(String.format("La materia %s non esiste", codiceMateria));
		}
		
		Page<Subject> result = subjectRepository.findQuery(nomeMateria, codiceMateria, cfu, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono ancora presenti materie");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}

	@Override
	public Subject findById(long id) throws NotFoundException {
		return Optional.ofNullable(subjectRepository.findById(id))
					   .orElseThrow(() -> new NotFoundException(String.format("L'id %s è inesistente", id)));
	}

	@Override
	public Subject save(Subject s) throws BadRequestException, ConflictException {
		s.validateSubject();
		
		Subject prev = subjectRepository.findByCodiceMateriaAndIdIsNot(s.getCodiceMateria(), s.getId());
		PageUtils.isExisting(prev);
		
		
		return subjectRepository.save(s);
	}

	@Override
	public Subject update(long id, Subject s) throws Exception{
		Subject prev = findById(id);
		
		if(!prev.getCodiceMateria().equalsIgnoreCase(s.getCodiceMateria())) {
			prev = subjectRepository.findByCodiceMateria(s.getCodiceMateria());
			PageUtils.isExisting(prev);
		}
		
		s.setId(id);
		return save(s);
	}

	@Transactional
	@Override
	public void delete(long id) throws NotFoundException {
		findById(id);
		
		subjectRepository.deleteById(id);
	}
	
	

}
