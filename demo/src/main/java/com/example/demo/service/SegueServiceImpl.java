package com.example.demo.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.example.demo.classes.Course;
import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Segue;
import com.example.demo.classes.Student;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.SegueRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.utils.PageUtils;

@Service
public class SegueServiceImpl implements SegueService{
	
	private final StudentRepository studentRepository;
	private final CourseRepository courseRepository;
	private final SegueRepository segueRepository;
	
	@Value("${demo.default-limit}")
	private Integer defaultLimit;
	
	@Value("${demo.max-limit}")
	private Integer maxLimit;
	
	public SegueServiceImpl(CourseRepository courseRepository, StudentRepository studentRepository, SegueRepository segueRepository) {
	      this.studentRepository = studentRepository;
	      this.courseRepository = courseRepository;
	      this.segueRepository = segueRepository;
	}
	
	@Override
	public PaginatedResponse<Segue> findAllCourseByStudent(String matricola, String nomeCorso, String sort, Integer page, Integer limit) throws BadRequestException, NotFoundException, NoContentException{
		page = Optional.ofNullable(page).orElse(0);
		limit = Optional.ofNullable(limit).orElse(defaultLimit);
		
		if(limit > maxLimit) throw new BadRequestException(String.format("Il valore massimo per limit è: %s", maxLimit));
		
		Sort s = PageUtils.buildSorting(sort);
		
		PageRequest pageRequest = null;
		
		if(s != null) {
			pageRequest = PageRequest.of(page, limit, s);
		}else {
			pageRequest = PageRequest.of(page, limit);
		}
		
		if(matricola != null) {
			Student stud = studentRepository.findFirstByMatricola(matricola);
			if(stud == null) throw new NotFoundException(String.format("Lo studente con matricola %s non esiste", matricola));
		}
		if(nomeCorso != null) {
			nomeCorso = "%" + nomeCorso + "%";
		}
		
		Page<Segue> result = segueRepository.findAllCourseByStudent(matricola, nomeCorso, pageRequest);
		
		if(result.getContent().size() == 0) throw new NoContentException("Non sono presenti ancora nessun associazione tra corso e studente");
		
		return new PaginatedResponse<>(result.getContent(), page, result.getTotalElements());
	}
	
	@Override
	public Segue findById(long idC, long idS) throws NotFoundException {
		return Optional.ofNullable(segueRepository.findFirstByIdCorsoAndIdStudente(idC, idS))
					   .orElseThrow(() -> new NotFoundException("Lo studente non partecipa al corso inserito"));
	}

	@Override
	public Segue save(Segue s) throws NotFoundException, BadRequestException, ConflictException {
		Student prev = studentRepository.findFirstById(s.getIdStudente());
		if(prev == null) throw new NotFoundException("Lo studente non esiste");
		Course prev2 = courseRepository.findFirstById(s.getIdCorso());
		if(prev2 == null) throw new NotFoundException("Il corso non esiste");
		
		if(!prev.getUniversity().getName().equalsIgnoreCase(prev2.getUniversity().getName())) throw new BadRequestException("Lo studente non può frequentare corsi di università a cui non è iscritto");
		
		Segue exist = segueRepository.findFirstByIdCorsoAndIdStudente(s.getIdCorso(), s.getIdStudente());
		if(exist != null) throw new ConflictException(String.format("L'associazione tra lo studente con id %s e il corso con id %s esiste già", s.getIdStudente(), s.getIdCorso()));

		if(!prev.getCourses().isEmpty()) {
			for(Course c : prev.getCourses()) {
				if(prev2.getAmbito().equalsIgnoreCase(c.getAmbito())) {
					throw new BadRequestException("Lo studente è gia iscritto ad un corso nello stesso ambito");
				}
			}
		}
		
		return segueRepository.save(s);
	}

	@Transactional
	@Override
	public void remove(long idC, long idS) throws NotFoundException {
		findById(idC, idS);
		
		Course c = courseRepository.findFirstById(idC);
		Student s = studentRepository.findFirstById(idS);
		
		if(c == null) throw new NotFoundException("Il corso non esiste");
		if(s == null) throw new NotFoundException("Lo studente non esiste");
		
		s.removeCourse(c);
		segueRepository.deleteByIdCorsoAndIdStudente(idC, idS);
		
	}
}
