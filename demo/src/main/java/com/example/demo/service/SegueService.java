package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.classes.PaginatedResponse;
import com.example.demo.classes.Segue;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.NotFoundException;

@Service
public interface SegueService {
	
	PaginatedResponse<Segue> findAllCourseByStudent(String matricola, String nomeCorso, String sort, Integer page, Integer limit) throws Exception;
	Segue findById(long idC, long idS) throws NotFoundException;
	Segue save(Segue s) throws NotFoundException, BadRequestException, ConflictException;
	void remove(long idC, long idS) throws NotFoundException;
}
