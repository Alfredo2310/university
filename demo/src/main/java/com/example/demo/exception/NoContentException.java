package com.example.demo.exception;

public class NoContentException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public NoContentException(String message) {
		super(message);
	}

}
