package com.example.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.classes.Course;

@Repository
public interface CourseRepository extends PagingAndSortingRepository<Course, Long>{

	Course findFirstById(Long id);
	Course findFirstByName(String name);
	Course findFirstByNameAndUniversityNameAndIdIsNot(String name, String Uniname, Long id);
	List<Course> findAllByDurata(Integer durata);
	List<Course> findAllByAmbito(Integer ambito);
	Course findFirstByNameAndUniversityName(String nomeCorso, String name);
	Course findByAmbito(String ambito);
	Course findByDurata(Integer durata);
	
	@Query(value = " SELECT cours " +
				   " FROM Course cours, Segue seg, Student s " +
				   " WHERE cours.id = seg.idCorso " +
				   " AND s.id = seg.idStudente " +
				   " AND s.id = :idS " )
	Course findCourseByIdStudent(@Param("idS") long idS);
	
	@Query(value = " SELECT cou " +
				   " FROM Course cou " +
				   " WHERE ( cou.name like :name or :name is null ) " + 
				   " AND ( cou.durata = :durata or :durata is null ) " +
				   " AND ( cou.ambito = :ambito or :ambito is null ) " +
				   " AND ( cou.university.name = :nameUni or :nameUni is null ) " )
	Page<Course> findQuery(@Param("name") String nomeCorso,
						   @Param("ambito") String ambito,
						   @Param("durata") Integer durata,
						   @Param("nameUni") String name,
						   Pageable pageable);
}
