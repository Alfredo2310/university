package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.classes.Contiene;

@Repository
public interface ContieneRepository extends PagingAndSortingRepository<Contiene, Long>{
	
	Contiene findFirstByIdCorsoAndIdMateria(long idCorso, long idMateria);
	
	@Query(value = " SELECT cont " +
				   " FROM Contiene cont, Course cou, Subject su " +
				   " WHERE ( cont.idCorso = cou.id ) " +
				   " AND ( cont.idMateria = su.id ) " +
				   " AND ( cou.name = :corso or :corso is null )"+ 
				   " AND ( su.codiceMateria = :codeSub or :codeSub is null ) " +
				   " AND ( cou.university.name = :nameUni or :nameUni is null )")
	Page<Contiene> findQuery(@Param("corso") String nameCorso,
							 @Param("codeSub") String codeSub,
							 @Param("nameUni") String nameUni,
							 Pageable pageable);
	
}
