package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.classes.Subject;

@Repository
public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long>{

	Subject findFirstById(Long id);
	Subject findById(long id);
	Subject findByCodiceMateria(String codiceMateria);
	Subject findByCodiceMateriaAndIdIsNot(String codiceMateria, Long id);
	Subject findFirstBynomeMateria(String nomeMateria);
	
	@Query(value = "SELECT sub " +
			   " FROM Subject sub " +
			   " WHERE ( sub.nomeMateria = :nome or :nome is null ) " + 
			   " and ( sub.codiceMateria like :code or :code is null ) " +
			   " and ( sub.cfu = :cfu or :cfu is null )" )
	Page<Subject> findQuery(@Param("nome") String nomeMateria,
							   @Param("code") String codiceMateria,
							   @Param("cfu") Integer cfu,
							   Pageable pageable);

}
