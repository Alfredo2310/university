package com.example.demo.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.classes.State;

@Repository
public interface StateRepository extends PagingAndSortingRepository<State, Integer>{
	
	State findFirstByCode(String code);
	State findFirstById(long id);
	State findFirstByName(String name);
	State findFirstByCodeAndIdIsNot(String code, long id);
	void deleteById(long id);
	
	@Query(value = "SELECT st " +
				   " FROM State st " +
				   " WHERE (st.name = :name or :name is null) " +
				   " and (st.code = :code or :code is null) " )
	Page<State> findQuery(@Param("code") String code,
						 @Param("name") String name,
						 Pageable pageable);
} 
