package com.example.demo.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.classes.Segue;

@Repository
public interface SegueRepository extends PagingAndSortingRepository<Segue, Long>{

	Segue findFirstByIdCorsoAndIdStudente(long idCorso, long idStudente);
	void deleteByIdCorsoAndIdStudente(long idCorso, long idStudente);
	Segue findTopByOrderByIdAsc();
	
	@Query(value = " SELECT cours, stud " +
			   " FROM Course cours, Segue seg, Student stud " +
			   " WHERE ( cours.id = seg.idCorso ) " + 
			   " AND ( seg.idStudente = stud.id ) " +
			   " AND ( stud.matricola = :matricola or :matricola is null) " +
			   " AND ( cours.name like :name or :name is null ) " )
Page<Segue> findAllCourseByStudent(@Param("matricola") String matricola, 
									@Param("name") String nomeCorso,
									Pageable pageable);
}
