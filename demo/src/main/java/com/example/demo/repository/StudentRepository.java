package com.example.demo.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.classes.Student;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student, Integer>{
	
	Student findFirstById(Long id);
	Student findFirstByName(String name);
	Student findFirstBySurname(String surname);
	Student findFirstByMatricola(String m);
	Student findByMatricolaAndIdIsNot(String matricola, Long id);
	Student findFirstByBirth(LocalDate birth);
	void deleteById(long id);
	
	@Query(value = " SELECT stud " +
				   " FROM Student stud " +
				   " WHERE ( stud.matricola like :matricola or :matricola is null ) " + 
				   " AND ( stud.name = :name or :name is null ) " +
				   " AND ( stud.surname = :surname or :surname is null ) " + 
				   " AND ( stud.birth = :birth or :birth is null ) " +
				   " AND ( stud.dateRegistration = :reg or :reg is null ) " +
				   " AND ( stud.university.name = :nomeUni or :nomeUni is null ) " )
	Page<Student> findQuery(@Param("matricola") String matricola,
							@Param("name") String name,
							@Param("surname") String surname,
							@Param("nomeUni") String nomeUni,
							@Param("birth") LocalDate birth,
							@Param("reg") LocalDate dateRegistration,
							Pageable pageable);
	
	@Query(value = " SELECT stud " +
				   " FROM Student stud, Course cours, Segue seg " +
				   " WHERE ( stud.id = seg.idStudente ) " +
				   " AND ( cours.id = seg.idCorso ) " +
				   " AND ( cours.name like :nome or :nome is null ) ")
	Page<Student> findAllStudentByNameCorso(@Param("nome") String nomecorso, Pageable pageable);
}
