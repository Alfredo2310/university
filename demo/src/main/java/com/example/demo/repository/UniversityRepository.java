package com.example.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.classes.University;

@Repository
public interface UniversityRepository extends PagingAndSortingRepository<University, Integer>{
	
	University findFirstByName(String name);
	University findFirstById(long id);
	University findFirstByAddress(String address);
	University findFirstByAddressAndName(String address, String name);
	University findFirstByNameAndIdIsNot(String name, long id);
	void deleteById(long id);
	
	@Query(value = "SELECT uni " +
			   " FROM University uni, State s " +
			   " WHERE ( uni.state.id = s.id ) " +
			   " AND ( uni.address = :address or :address is null ) " + 
			   " AND ( uni.name = :name or :name is null ) " + 
			   " AND ( uni.state.code = :codeS or :codeS is null ) " )
	Page<University> findQuery(@Param("address") String address,
							   @Param("name") String name,
							   @Param("codeS") String code,
							   Pageable pageable);
}