package com.example.demo.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.classes.Course;
import com.example.demo.classes.LibUni;
import com.example.demo.classes.Subject;

public interface LibUniRepository extends PagingAndSortingRepository<LibUni, Long>{

	LibUni findFirstById(Long id);
	LibUni findFirstByCourseIdAndSubjectId(Course c, Subject s);
	void deleteByStudentIdAndCourseIdAndSubjectId(Long idStudent, Long idCourse, Long idSubject);
	
	
	@Query(value = " SELECT lib " + 
				   " FROM LibUni lib " +
				   " WHERE ( lib.student.id = :stud ) " +	
				   " AND ( lib.subject.nomeMateria = :sub ) " +
				   " AND ( lib.course.id = :course ) " +
				   " AND ( lib.voto > 17 ) ")
	LibUni findExamPassed(@Param("sub") String s, @Param("stud") Long idStud, @Param("course") Long idCou);
	
	@Query(value = " SELECT lib " + 
				   " FROM LibUni lib " +
				   " WHERE ( lib.student.id = :student ) " +
				   " AND ( lib.course.id = :course ) " +
				   " AND ( lib.subject.id = :subject ) " )
	LibUni findExam(@Param("student") Long idStud, @Param("course") Long idCou, @Param("subject") Long idSub);
	
	@Query(value = " SELECT lib " +
				   " FROM LibUni lib " +
				   " WHERE ( lib.subject.codiceMateria = :subject or :subject is null ) " +
				   " AND ( lib.student.matricola = :student or :student is null) " +
				   " AND ( lib.date = :date or :date is null ) " +
				   " AND ( lib.course.name = :course or :course is null ) " )
	Page<LibUni> findQuery( @Param("subject") String subject,
							@Param("course") String course,
							@Param("date") LocalDate date,
							@Param("student") String matricola,
						   Pageable pageable);
}
