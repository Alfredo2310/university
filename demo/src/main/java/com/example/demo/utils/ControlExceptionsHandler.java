package com.example.demo.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ForbiddenException;
import com.example.demo.exception.NoContentException;
import com.example.demo.exception.NotFoundException;

@ControllerAdvice
public class ControlExceptionsHandler {
	
	@ExceptionHandler(value = {NotFoundException.class})
	protected ResponseEntity<ErrorResponse> handleNotFound(NotFoundException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()),HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = {BadRequestException.class})
	protected ResponseEntity<ErrorResponse> handleBadRequest(BadRequestException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()),HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = {ConflictException.class})
	protected ResponseEntity<ErrorResponse> handleConflict(ConflictException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()),HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(value = {ForbiddenException.class})
	protected ResponseEntity<ErrorResponse> handleConflict(ForbiddenException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()),HttpStatus.FORBIDDEN);
	}
	
	@ExceptionHandler(value = {NoContentException.class})
	protected ResponseEntity<ErrorResponse> handleConflict(NoContentException ex){
		return new ResponseEntity<>(new ErrorResponse(ex.getMessage()),HttpStatus.OK);
	}
}
