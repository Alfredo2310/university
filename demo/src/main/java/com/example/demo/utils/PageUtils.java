package com.example.demo.utils;

import org.springframework.data.domain.Sort;
import com.example.demo.classes.Course;
import com.example.demo.classes.State;
import com.example.demo.classes.Student;
import com.example.demo.classes.Subject;
import com.example.demo.classes.University;
import com.example.demo.exception.ConflictException;

public class PageUtils {

	public static Sort buildSorting(String sort) {
		Sort s1 = null;
		
		if( sort != null) {
			String[] s2 = sort.split(",");
			for(String s : s2) {
				Sort.Direction dir = s.contains("-") ? Sort.Direction.DESC : Sort.Direction.ASC;
				if(s1 == null) {
					s1 = Sort.by(dir, s.replace("-", ""));
				}else {
					s1.and(Sort.by(dir, s.replace("-", "")));
				}
			}
		}
		return s1;
	}
	
	public static Object isExisting(Object o) throws ConflictException {
		
		if(o instanceof Student) {
			if(o != null) {o = null; throw new ConflictException("La matricola è già esistente");}
		}else if(o instanceof University) {
			if(o != null) {o = null; throw new ConflictException("L'università è già esistente");} 
		}else if(o instanceof State) {
			if(o != null) { o = null; throw new ConflictException("Lo stato è già esistente");}
		}else if(o instanceof Subject) { 
			if(o != null) { o = null; throw new ConflictException("La materia è gia esistente");}
		}else if(o instanceof Course) {
			if(o != null) { o = null; throw new ConflictException("Il corso è gia esistente");}
		}
		return o;
	}
	
	public static String format(String name) {
		if(name != null) {
			name = name.trim();
			if(name.contains(" ")) {
				String word1 = name.substring(0, name.indexOf(" ")).toUpperCase().trim();
				String word2 = name.substring(name.indexOf(" ")+1, name.length()).toUpperCase().trim();
				char word1Upper = word1.charAt(0);
				char word2Upper = word2.charAt(0);
				word1 = word1.replace(word1.charAt(0), word1Upper);
				word2 = word2.replace(word2.charAt(0), word2Upper);
				name = word1 + word2;
 			}else {
				name = name.toUpperCase(); 
				char app = name.charAt(0);
				String app2 = name.substring(1, name.length()).toLowerCase();
				name = app + app2;
			}
		}
		return name;
	}
	
	public static boolean containsNumber(String name) {
		String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		for(String c : numbers) {
			if(name.contains(c)) {
				return true;
			}
		}
		return false;
	}
}
